container_name = carnet-web-site

start:
	docker run -it --rm --name $(container_name) -p 8443:443 -v `pwd`/site:/usr/local/apache2/htdocs/ rdroro/apache-local

start-node:
	docker run -it --rm --name $(container_name)-node -v `pwd`:/opt/app node:12-buster bash
